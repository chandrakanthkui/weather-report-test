# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Weather Report Application
* 1.0


				
Module:  	weatherApp
Services :	ApiCall
Controller :	 cityWeatherController
Variables:	
	forecastShow – boolean – for displaying the forecast data
	backShow – Boolean – to display the back button in forecast view
	cities – Array – containes the each cities’ weather report.
	fcData – Array – containes the selected city’s 5 day foreast weather report.
Variables in Service (Private) : 
•	apiCallFunc – object – returns the data collected from response of Api call.
•	Get – function belongs to apiCallFunc object which internally called using the apiCallFunc.
•	currDate – date – date and day of the weather 
•	type – string – selected type of weathere report (can be fc – forecast or curr – current )
•	imgSrc – string – to get the source of the weather report image 
For displaying the current weather of selected 5 cities:
	ApiCall factory is used to call the api which returns the weather report of the selected 5 cities.
	Cities is the array used to collect the data returned from the ApiCall factory.
	The cities is iterated in the view to show the collected information from the api.
	Since the forecast view and back button in forecast view should be hidden, the foreCastShow and backShow are assigned with false.
For displaying the 5 day forecast weather of selected 5 cities:
	ApiCall factory is used to call the api which returns the 5 day forecast weather report  of the selected city.
	fcData is the array used to collect the data returned from the ApiCall factory.
	The fcData is iterated in the view to show the collected information from the api.
	Since the forecast view and back button in forecast view should be shown, the foreCastShow and backShow are assigned with true.
ApiCall Service method: 
Factory method is injected with the built in service $http which is used to call the api.
apiCallFunc is an object contains get method which returns a promise object in the success of the api call.
It receives the information type of the requested data whether current weather info or 5 day forecast information. By comparing with information type (fc or curr) the required data will be collected and stored in the array called data.
Styles:
Bootstrap is used for grid layout, buttons and display of labels.
View:
Html5 is used to display the day.