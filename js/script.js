 
 /*Angular module creation*/
 var weatherApp = angular.module('weatherApp',[]);
				
				/*controller for displaying current weather report of 5 cities*/
				weatherApp.controller('cityWeatherController',function($scope,ApiCall){
					$scope.forecastShow = false;
					$scope.backShow=false;
					$scope.cities=[];					
					$scope.cities= ApiCall.get("http://api.openweathermap.org/data/2.5/find?lat=44.5260&lon=8.2551&cnt=5&units=metric&appid=3d8b309701a13f65b660fa2c64cdc517",'curr');					
					/*registering click event for forecast data */
					$scope.forecast= function(fc,id){
						$scope.forecastShow = true;
						$scope.fcData=[];					
						$scope.fcData= ApiCall.get("http://api.openweathermap.org/data/2.5/forecast?id="+id+"&units=metric&appid=3d8b309701a13f65b660fa2c64cdc517",fc,id);						
						
						/*displaying the back button in a delay after rendering the 5 day forecast data in the page*/
						setTimeout(function(){
							$scope.backShow=true;
						},100);
						
						/*registering back button click event*/
						$scope.backToCities = function(){
							$scope.forecastShow = false;
							$scope.backShow=false;
						}
					}
				});
				
				
				/*Service for api call for receiving data of weather which returns a promise object*/
				weatherApp.factory('ApiCall',function($http){
						var apiCallFunc ={
							get: function(path,type){ /*path is api path and type is to know the requested is foreecast or current weather*/
								var data=[],imgSrc;								
								$http.get(path)
								.success(function(response) {
									if(response){
									angular.forEach(response.list, function(value, key) {
									  if(type == 'fc'){										  
										  var currDate = new Date(value.dt_txt),isToday=false;																						  
										  if(currDate.getHours() == '12' && currDate.getMinutes() == '0'){			
												/*To check day is today*/
												if(currDate.setHours(0,0,0,0) == new Date().setHours(0,0,0,0)){
													isToday = true;
												}
												switch(value.weather[0].description){
													case 'clear sky':imgSrc = 'images/clear_sky.png';
																	 break;
													case 'light rain':imgSrc = 'images/light_rain.png';
																	 break;
													case 'broken clouds':imgSrc = 'images/cloud.png';
																	 break;
												}
											  data.push({
												  'date': new Date(value.dt_txt),
												  'desc': value.weather[0].description,
												  'temp_min': value.main.temp_min,
												  'temp_max' : value.main.temp_max,
												  'pressure': value.main.pressure,
												  'clouds': value.main.humidity,
												  'temp': value.main.temp,
												  'wind': value.wind.speed,
												  'isToday' : isToday,
												  'img':imgSrc
											  });
										  }
									  }else{
										  switch(value.weather[0].description){
													case 'clear sky':imgSrc = 'images/clear_sky.png';
																	 break;
													case 'light rain':imgSrc = 'images/light_rain.png';
																	 break;
													case 'few clouds':imgSrc = 'images/cloud.png';
																	 break;
												}
										  data.push({
												'id' : value.id,
												'name': value.name,
												'temparature':value.main.temp,
												'wind': value.wind.speed,
												'img': imgSrc
											});
									  }										
									});	
									}									
								}).error(function(response,status){
									console.log(response);
									console.log(status);
								});								
							return data;
							}
						}
						return apiCallFunc;
					});
					
				
				
				
				